# DrawPad Pro Plus+

Windows desktop application for basic computer drawing.  Originally developed by Chris Colahan, Audrey LoVan, and Will Roberts of Simpson College.  

This is an open application.

# Prerequisites

To run this application you may need software that can compile C# programs. 