﻿using DrawPad.Classes;
using System.Windows.Shapes;
using System.Windows.Media;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Input;

namespace DrawPad.Tools
{
    public abstract class IDrawTool
    {
        private readonly PixelCanvas canvas;
        private Stack<UIElement> drawingStack = new Stack<UIElement>();
        private Stack<UIElement> undoneStack = new Stack<UIElement>();
        private Brush color;

        public IDrawTool(PixelCanvas canvas, Brush color)
        {
            this.canvas = canvas;
            SetColor(color);
        }

        protected Brush GetColor()
        {
            return color;
        }

        public abstract Cursor GetCursor();

        public UIElement GetLastDrawn()
        {
            return drawingStack.Peek();
        }

        public virtual void SetColor(Brush color)
        {
            if (color != null)
            {
                this.color = color;
            }
        }

        public void Draw(UIElement element)
        {
            if (element != null)
            {
                undoneStack.Clear();
                drawingStack.Push(element);
                canvas.Draw(element);
            }
        }

        public void Undo()
        {
            if (drawingStack.Count() > 0) {
                UIElement last = drawingStack.Pop();
                undoneStack.Push(last);
                canvas.Remove(last);
            }
        }

        public void Redo()
        {
            if (undoneStack.Count() > 0)
            {
                UIElement lastUndone = undoneStack.Pop();
                drawingStack.Push(lastUndone);
                canvas.Draw(lastUndone);
            }
        }

    }
}
