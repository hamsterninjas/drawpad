﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DrawPad.Classes;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows;

namespace DrawPad.Tools
{
    public abstract class StylusTool : IDrawTool
    {
        public int strokeThickness { get; set; }
        private Point previousPoint;

        public StylusTool(PixelCanvas canvas, Point startPoint, Brush color, int strokeThickness) : base(canvas, color)
        {
            previousPoint = startPoint;
            this.strokeThickness = strokeThickness;
        }

        public abstract Shape CreateSegment(Point previousPoint, Point nextPoint);
        /**
         * If newLine is true, Draw a curve from the last point to the next point.
         * Else start a new line a p.
         */
        public void DrawStroke(Point point, Boolean newLine)
        {
            if(!newLine)
            {
                Shape segment = CreateSegment(previousPoint, point);
                Draw(segment);
            }
            previousPoint = point;
        }

        public override Cursor GetCursor()
        {
            return Cursors.Cross;
        }
    }
}
