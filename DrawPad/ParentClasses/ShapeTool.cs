﻿using System;
using DrawPad.Classes;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows;

namespace DrawPad.Tools
{
    public abstract class ShapeTool : IDrawTool
    {
        public Boolean fill { get; set; }
        public int strokeThickness { get; set; }
        public Brush strokeColor { get; set; }

        public ShapeTool(PixelCanvas canvas, Brush color, Boolean fill, int strokeThickness, Brush strokeColor) : base(canvas, color)
        {
            this.fill = fill;
            this.strokeThickness = strokeThickness;
            this.strokeColor = strokeColor;
        }

        public abstract Drawable CreateShape(Point p1, Point p2, Brush color, Boolean fill, int strokeThickness, Brush strokeColor);

        private void SetTopLeft(Drawable shape)
        {
            Canvas.SetTop(shape.element, shape.topLeft.Y);
            Canvas.SetLeft(shape.element, shape.topLeft.X);
        }

        public void DrawShape(Point p1, Point p2)
        {
            Drawable shapeData = CreateShape(p1, p2, GetColor(), fill, strokeThickness, strokeColor);
            SetTopLeft(shapeData);
            Draw(shapeData.element);
        }

        public void UpdateLastShape(Point p1, Point p2)
        {
            FrameworkElement last = (FrameworkElement) GetLastDrawn();
            Drawable shapeData = CreateShape(p1, p2, GetColor(), fill, strokeThickness, strokeColor);
            FrameworkElement newShape = shapeData.element;

            Canvas.SetLeft(last, shapeData.topLeft.X);
            Canvas.SetTop(last, shapeData.topLeft.Y);
            
            last.Width = newShape.Width;
            last.Height = newShape.Height;

            if(last is Polygon)
            {
                ((Polygon)last).Points = ((Polygon)newShape).Points;
            }
        }

        public override Cursor GetCursor()
        {
            return Cursors.Cross;
        }

    }
}
