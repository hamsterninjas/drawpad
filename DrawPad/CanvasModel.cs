﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrawPad
{
    public class CanvasModel
    {
        private UInt32[,] pixelData;

        public CanvasModel(int width, int height)
        {
            pixelData = new UInt32[width, height];
        }

        public bool inCanvas(int x, int y)
        {
            return x > 0 && x < pixelData.GetLength(0) && y > 0 && y < pixelData.GetLength(1);
        }

        public void drawSquare(int x, int y, int width, int height, UInt32 color)
        {
            if(inCanvas(x, y) && inCanvas(x + width, y + height))
            {
                for(int xCntr = x; xCntr <= x + width; xCntr ++)
                {
                    for(int yCntr = y; yCntr <= y + width; yCntr ++)
                    {
                        pixelData[xCntr, yCntr] = color;
                    }
                }
            }
            else
            {
                throw new IndexOutOfRangeException("Coordinates out of range");
            }
        }
        
        /*
         * Uses the FNV-1a hash, a non-cryptographic hash, for speed.
         */ 
        /*public UInt64 getPixelDataHash()
        {
            const UInt64 FNVOffsetConstant = 14695981039346656037;
            const UInt64 FNVPrimeConstant = 1099511628211;

            UInt64 hash = FNVOffsetConstant;

            for (int x = 0; x < pixelData.GetLength(0); x ++)
            {
                for(int y = 0; y < pixelData.GetLength(1); y ++)
                {
                    //extract the 4 bytes from each 32 bit int
                    Byte byte1 = (Byte) (pixelData[x, y] >> 24);
                    Byte byte2 = (Byte) (pixelData[x, y] >> 16);
                    Byte byte3 = (Byte) (pixelData[x, y] >> 8);
                    Byte byte4 = (Byte) (pixelData[x, y]);

                    hash ^= byte1;
                    hash *= FNVPrimeConstant;
                    hash ^= byte2;
                    hash *= FNVPrimeConstant;
                    hash ^= byte3;
                    hash *= FNVPrimeConstant;
                    hash ^= byte4;
                    hash *= FNVPrimeConstant;
                }
            }
        }*/
    }
}
