﻿using System;
using System.Windows.Media;
using System.Windows.Shapes;
using DrawPad.Classes;
using System.Windows;

namespace DrawPad.Tools
{
    public class PenTool : StylusTool
    {
        public PenTool(PixelCanvas canvas, Point startPoint, Brush color, int strokeThickness) : base(canvas, startPoint, color, strokeThickness)
        {

        }

        public override Shape CreateSegment(Point previousPoint, Point nextPoint)
        {
            var line = new Line
            {
                StrokeThickness = strokeThickness,
                X1 = nextPoint.X,
                Y1 = nextPoint.Y,
                X2 = previousPoint.X,
                Y2 = previousPoint.Y,
                Stroke = GetColor()
            };

            return line;
        }
        
    }
}
