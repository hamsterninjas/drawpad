﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DrawPad.Classes;
using System.Windows;
using System.Windows.Shapes;
using System.Windows.Media;

namespace DrawPad.Tools
{
    public abstract class IDrawTool
    {
        private readonly PixelCanvas canvas;
        private Shape lastDrawnShape = null;
        public Brush color { get; set; }

        public IDrawTool(PixelCanvas canvas, Brush color)
        {
            this.canvas = canvas;
            this.color = color;
        }

        public Shape getLastDrawn()
        {
            return lastDrawnShape;
        }

        public void draw(Shape shape)
        {
            this.lastDrawnShape = shape;
            if (shape != null)
            {
                canvas.draw(shape);
            }
        }

        public void removeLastDrawn()
        {
            if (lastDrawnShape != null)
            {
                canvas.remove(lastDrawnShape);
                lastDrawnShape = null;
            }
        }

    }
}
