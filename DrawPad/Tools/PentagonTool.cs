﻿using System;
using DrawPad.Classes;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Windows;

namespace DrawPad.Tools
{
    class PentagonTool : ShapeTool
    {
        public PentagonTool(PixelCanvas canvas, Brush color, Boolean fill, int strokeThickness, Brush strokeColor) : base(canvas, color, fill, strokeThickness, strokeColor)
        {
        }

        public override Drawable CreateShape(Point p1, Point p2, Brush color, bool fill, int strokeThickness, Brush strokeColor)
        {
            Point upperPoint = new Point(Math.Min(p1.X, p2.X), Math.Min(p1.Y, p2.Y));

            var width = Math.Abs(p2.X - p1.X);
            var height = Math.Abs(p2.Y - p1.Y);

            //top point of pentagon
            var topX = width / 2.0;
            var topY = 0;

            //left bottom point of triangle
            var leftBX = width / 4;
            var leftBY = height;

            //left side of pentagon
            var leftSX = 0;
            var leftSY = height / 2.5;

            //right side of pentagon
            var rightSX = width;
            var rightSY = height / 2.5;

            //right bottom point of triangle
            var rightBX = (width / 4) * 3;
            var rightBY = height;

            var leftPoint = new Point(leftBX, leftBY);
            var leftSidePoint = new Point(leftSX, leftSY);
            var topPoint = new Point(topX, topY);
            var rightSidePoint = new Point(rightSX, rightSY);
            var rightPoint = new Point(rightBX, rightBY);
            var myPointCollection = new PointCollection();
            myPointCollection.Add(leftPoint);
            myPointCollection.Add(leftSidePoint);
            myPointCollection.Add(topPoint);
            myPointCollection.Add(rightSidePoint);
            myPointCollection.Add(rightPoint);

            var triangle = new Polygon
            {
                Width = width * 2,
                Height = height * 2,
                Fill = color,
                Points = myPointCollection,
                StrokeThickness = strokeThickness,
                Stroke = strokeColor,
            };

            return new Drawable(triangle, new Point(upperPoint.X, upperPoint.Y));
        }
    }
}

