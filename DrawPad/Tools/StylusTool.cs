﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DrawPad.Classes;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Windows.Input;
using System.Windows.Controls;

namespace DrawPad.Tools
{
    public abstract class StylusTool : IDrawTool
    {
        public int strokeThickness { get; set; }
        private Point previousPoint;

<<<<<<< HEAD
        private readonly int numUndo = 100;

=======
>>>>>>> WillBranch
        public StylusTool(PixelCanvas canvas, Point startPoint, Brush color, int strokeThickness) : base(canvas, color)
        {
            this.previousPoint = startPoint;
            this.strokeThickness = strokeThickness;
        }

        public abstract Shape createSegment(Point previousPoint, Point nextPoint);

        /**
         * If newLine is true, Draw a curve from the last point to the next point.
         * Else start a new line a p.
         */
        public void drawStroke(Point point, Boolean newLine, ToolsEnum tool)
        {
            if(!newLine)
            {
<<<<<<< HEAD
                Shape segment = createSegment(previousPoint, point);
                draw(segment);
=======
                if (tool == ToolsEnum.Brush)
                {
                    // Hypothetical brush example
                    var brushLine = new Line
                    {
                        StrokeThickness = strokeThickness,
                        X1 = point.X,
                        Y1 = point.Y,
                        X2 = point.X + 10,
                        Y2 = point.Y + 5,
                        Stroke = color
                    };

                    draw(brushLine);
                }

                else if (tool == ToolsEnum.Eraser)
                {
                    var rect = new Rectangle
                    {
                        Width = 10,
                        Height = 10,
                        Fill = Brushes.Yellow,
                    };

                    draw(rect);
                }

                else if (tool == ToolsEnum.Line || tool == ToolsEnum.Pencil)
                {
                    var line = new Line
                    {
                        StrokeThickness = strokeThickness,
                        X1 = point.X,
                        Y1 = point.Y,
                        X2 = previousPoint.X,
                        Y2 = previousPoint.Y,
                        Stroke = color
                    };

                    draw(line);
                }

>>>>>>> WillBranch
            }
            previousPoint = point;
        }

        public override Cursor getCursor()
        {
            return Cursors.Pen;
        }

        public override void undo()
        {
            for(int i = 0; i < numUndo; i ++)
            {
                base.undo();
            }
        }

        public override void redo()
        {
            for (int i = 0; i < numUndo; i++)
            {
                base.redo();
            }
        }
    }
}
