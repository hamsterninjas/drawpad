﻿using DrawPad.Classes;
using System.Windows.Shapes;
using System.Windows.Media;
using System;
using System.Windows;

namespace DrawPad.Tools
{
    class RectangleTool : ShapeTool
    {

        public RectangleTool(PixelCanvas canvas, Brush color, Boolean fill, int strokeThickness, Brush strokeColor) : base(canvas, color, fill, strokeThickness, strokeColor)
        {
        }
        
        public override Drawable CreateShape(Point p1, Point p2, Brush color, bool fill, int strokeThickness, Brush strokeColor)
        {
            var width = Math.Abs(p1.X - p2.X);
            var height = Math.Abs(p1.Y - p2.Y);
            var x = Math.Min(p1.X, p2.X);
            var y = Math.Min(p1.Y, p2.Y);

            var rectangle = new Rectangle
            {
                Width = Math.Abs(width),
                Height = Math.Abs(height),
                Fill = color,
                Stroke = strokeColor,
                StrokeThickness = strokeThickness,
            };

            return new Drawable(rectangle, new Point(x,y));
        }
        
    }
}
