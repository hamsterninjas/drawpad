﻿using System;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using DrawPad.Classes;
using System.Windows;

namespace DrawPad.Tools
{
    class EllipseTool : ShapeTool
    {
        public EllipseTool(PixelCanvas canvas, Brush color, bool fill, int strokeThickness, Brush strokeColor) : base(canvas, color, fill, strokeThickness, strokeColor)
        {

        }
        
        public override Drawable CreateShape(Point p1, Point p2, Brush color, bool fill, int strokeThickness, Brush strokeColor)
        {
            var width = p2.X - p1.X;
            var height = p2.Y - p1.Y;
            var x = p2.X - width;
            var y = p2.Y - height;

            if (width < 0)
            {
                x = p2.X;
            }
            if (height < 0)
            {
                y = p2.Y;
            }

            var ellipse = new Ellipse
            {
                Width = Math.Abs(width),
                Height = Math.Abs(height),
                Fill = color,
                StrokeThickness = strokeThickness,
                Stroke = strokeColor,
            };

            return new Drawable(ellipse, new Point(x, y));
        }
    }
}
