﻿using DrawPad.Classes;
using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;

namespace DrawPad.Tools
{
    public class PenPathTool : ShapeTool
    {
        public PenPathTool(PixelCanvas canvas, Brush color, bool fill, int strokeThickness, Brush strokeColor) : base(canvas, color, fill, strokeThickness, strokeColor)
        {

        }

        public override Drawable CreateShape(Point p1, Point p2, Brush color, bool fill, int strokeThickness, Brush strokeColor)
        {
            // Issues lie with starting and ending points and the way we do previousPoint and nextPoint

            var width = p2.X - p1.X;
            var height = p2.Y - p1.Y;
            var x = p2.X - width;
            var y = p2.Y - height;

            if (width < 0)
            {
                x = p2.X;
            }
            if (height < 0)
            {
                y = p2.Y;
            }

            var myPathFigure = new PathFigure();
            myPathFigure.StartPoint = new System.Windows.Point(width, height);

            var myLineSegment = new LineSegment();
            // This is the problem
            myLineSegment.Point = new System.Windows.Point(x, y);

            var myPathSegmentCollection = new PathSegmentCollection();
            myPathSegmentCollection.Add(myLineSegment);

            myPathFigure.Segments = myPathSegmentCollection;

            var myPathFigureCollection = new PathFigureCollection();
            myPathFigureCollection.Add(myPathFigure);

            var myPathGeometry = new PathGeometry();
            myPathGeometry.Figures = myPathFigureCollection;

            var myPath = new Path();
            myPath.Stroke = GetColor();
            myPath.StrokeThickness = 7;
            myPath.Data = myPathGeometry;

            return new Drawable(myPath, new Point(x, y));
        }
    }
}
