﻿using System;
using DrawPad.Classes;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Windows;

namespace DrawPad.Tools
{
    class PolygonTool : ShapeTool
    {
        private int numOfSides;
        private double width;
        private double height;
        private PixelCanvas testCanvas;

        public PolygonTool(PixelCanvas canvas, Brush color, Boolean fill, int strokeThickness, Brush strokeColor, int sides) : base(canvas, color, fill, strokeThickness, strokeColor)
        {
            numOfSides = sides;
            testCanvas = canvas;
        }

        public PolygonTool(PixelCanvas canvas, Brush color, bool fill, int strokeThickness, Brush strokeColor) : base(canvas, color, fill, strokeThickness, strokeColor)
        {
        }

        public override Drawable CreateShape(Point p1, Point p2, Brush color, bool fill, int strokeThickness, Brush strokeColor)
        {
            Point upperPoint = new Point(Math.Min(p1.X, p2.X), Math.Min(p1.Y, p2.Y));

            var myPointCollection = new PointCollection();

            width = Math.Abs(p2.X - p1.X);
            height = Math.Abs(p2.Y - p1.Y);

            for (int i = 0; i < numOfSides; i++)
            {
                Point temp;

                if (numOfSides % 2 == 0)
                {
                    temp = GenerateFlatTop(i);
                }
                else
                {
                    temp = GenerateTopPoint(i);
                }

                myPointCollection.Add(temp);
            }

            //var left1 = new Point(1, 50);
            //var left2 = new Point(25, 100);
            //var topPoint = new Point(51, 10);
            //var right1 = new Point(101, 50);
            //var right2 = new Point(75, 100);

            //myPointCollection.Add(left2);
            //myPointCollection.Add(left1);
            //myPointCollection.Add(topPoint);
            //myPointCollection.Add(right1);
            //myPointCollection.Add(right2);

            var triangle = new Polygon
            {
                Width = width * 2,
                Height = height * 2,
                Fill = color,
                Points = myPointCollection,
                StrokeThickness = strokeThickness,
                Stroke = strokeColor,
            };

            return new Drawable(triangle, new Point(upperPoint.X, upperPoint.Y));
        }

        private Point GenerateTopPoint(int point)
        {
            double x = 0;
            double y = 0;

            double median = (numOfSides / 2);

            if (point == median)
            {
                //top point of triangle
                x = width / 2.0;
                y = 0;
            }

            else if (point == 0)
            {
                //left point of triangle
                x = 0;
                y = height;
            }

            else if (point == numOfSides - 1)
            {
                //right point of triangle
                x = width;
                y = height;
            }

            return new Point(x, y);
        }

        private Point GenerateFlatTop(int points)
        {
            var median1 = numOfSides / 2;
            var median2 = median1 - 1;

            double x = 0;
            double y = 0;

            if (points == median1 || points == median2)
            {
                x = 0;
                y = 0;
            }
            return new Point(x, y);
        }
    }
}

