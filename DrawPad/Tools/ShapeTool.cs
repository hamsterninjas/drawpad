﻿using System;
using DrawPad.Classes;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Windows.Controls;

namespace DrawPad.Tools
{
    public abstract class ShapeTool : IDrawTool
    {
        public Boolean fill { get; set; }

        public ShapeTool(PixelCanvas canvas, Brush color, Boolean fill) : base(canvas, color)
        {
            this.fill = fill;
        }

        public abstract Tuple<Shape, Point> createShape(Point p1, Point p2, Brush color, Boolean fill);

        public void drawShape(Point p1, Point p2)
        {
            Tuple<Shape, Point> shapeData = createShape(p1, p2, color, fill);
            Canvas.SetLeft(shapeData.Item1, shapeData.Item2.X);
            Canvas.SetTop(shapeData.Item1, shapeData.Item2.Y);
            draw(shapeData.Item1);
        }

        public void updateLastShape(Point p1, Point p2)
        {
            Shape last = getLastDrawn();
            Tuple<Shape, Point> shapeData = createShape(p1, p2, color, fill);
            Canvas.SetLeft(last, shapeData.Item2.X);
            Canvas.SetTop(last, shapeData.Item2.Y);

            last.Width = shapeData.Item1.Width;
            last.Height = shapeData.Item1.Height;
        }
    }
}
