﻿using System;
using DrawPad.Classes;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Windows;

namespace DrawPad.Tools
{
    class TriangleTool : ShapeTool
    {
        public TriangleTool(PixelCanvas canvas, Brush color, Boolean fill, int strokeThickness, Brush strokeColor) : base(canvas, color, fill, strokeThickness, strokeColor)
        {
        }

        public override Drawable CreateShape(Point p1, Point p2, Brush color, bool fill, int strokeThickness, Brush strokeColor)
        {
            Point upperPoint = new Point(Math.Min(p1.X, p2.X), Math.Min(p1.Y, p2.Y));
            
            var width = Math.Abs(p2.X - p1.X);
            var height = Math.Abs(p2.Y - p1.Y);

            //top point of triangle
            var topX = width / 2.0;
            var topY = 0;

            //left point of triangle
            var leftX = 0;
            var leftY = height;

            //right point of triangle
            var rightX = width;
            var rightY = height;

            var leftPoint = new Point(leftX, leftY);
            var topPoint = new Point(topX, topY);
            var rightPoint = new Point(rightX, rightY);
            var myPointCollection = new PointCollection();
            myPointCollection.Add(leftPoint);
            myPointCollection.Add(topPoint);
            myPointCollection.Add(rightPoint);

            var triangle = new Polygon
            {
                Width = width,
                Height = height,
                Fill = color,
                Points = myPointCollection,
                StrokeThickness = strokeThickness,
                Stroke = strokeColor,
            };

            return new Drawable(triangle, new Point(upperPoint.X, upperPoint.Y));
        }
    }
}
