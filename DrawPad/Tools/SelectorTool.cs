﻿using DrawPad.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Input;

namespace DrawPad.Tools
{
    class SelectorTool : IDrawTool
    {
        public SelectorTool(PixelCanvas canvas) : base(canvas, Brushes.Black)
        {

        }

        public override Cursor GetCursor()
        {
            return Cursors.Arrow;
        }
    }
}
