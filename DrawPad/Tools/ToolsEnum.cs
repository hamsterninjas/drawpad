﻿using System.ComponentModel.DataAnnotations;

namespace DrawPad.Tools
{
    public enum ToolsEnum
    {
        [Display(Name = "Selector Tool")]
        Arrow = 0,

        [Display(Name = "Pen Tool")]
        Line = 1,

        [Display(Name = "Rectangle Tool")]
        Rect = 2,

        [Display(Name = "Eraser Tool")]
        Eraser = 3,

        [Display(Name = "Brush Tool")]
        Brush = 4,

        [Display(Name = "Ellipse Tool")]
        Ellipse = 5,

        [Display(Name = "Pencil Tool")]
        Pencil = 6,
    }
}
