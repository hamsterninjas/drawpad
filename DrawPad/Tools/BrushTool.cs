﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DrawPad.Classes;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Windows;

namespace DrawPad.Tools
{
    class BrushTool : StylusTool
    {
        public BrushTool(PixelCanvas canvas, Point startPoint, Brush color, int strokeThickness) : base(canvas, startPoint, color, strokeThickness)
        {
            
        }

        public override Shape CreateSegment(Point previousPoint, Point nextPoint)
        {
            var brushLine = new Line
            {
                StrokeThickness = strokeThickness,
                X1 = nextPoint.X,
                Y1 = nextPoint.Y,
                X2 = nextPoint.X + 10,
                Y2 = nextPoint.Y + 5,
                Stroke = GetColor()
            };

            return brushLine;
        }
    }
}
