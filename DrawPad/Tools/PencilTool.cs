﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DrawPad.Classes;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Windows;

namespace DrawPad.Tools
{
    public class PencilTool : StylusTool
    {
        public PencilTool(PixelCanvas canvas, Point startPoint) : base(canvas, startPoint, Brushes.Gray, 1)
        {
            // Pencil Tool has set stroke thickness of 1 and color of light gray
        }

        public override Shape CreateSegment(Point previousPoint, Point nextPoint)
        {
            var line = new Line
            {
                StrokeThickness = strokeThickness,
                X1 = nextPoint.X,
                Y1 = nextPoint.Y,
                X2 = previousPoint.X,
                Y2 = previousPoint.Y,
                Stroke = GetColor()
            };

            return line;
        }
        
    }
}
