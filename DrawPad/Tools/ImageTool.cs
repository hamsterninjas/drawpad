﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Shapes;
using DrawPad.Classes;
using System.Windows.Media.Imaging;
using System.Windows.Controls;
using System.Windows;

namespace DrawPad.Tools
{
    class ImageTool : ShapeTool
    {
        private readonly string path;
        private readonly BitmapImage bitmap;

        public ImageTool(PixelCanvas canvas, string path) : base(canvas, Brushes.Black, false, 1, Brushes.Black)
        {
            this.path = path;
            bitmap = new BitmapImage(new Uri(path));
        }

        public override Drawable CreateShape(Point p1, Point p2, Brush color, bool fill, int strokeThickness, Brush strokeColor)
        {
            Image img = new Image();
            img.Source = bitmap;
            img.Width = Math.Abs(p1.X - p2.X);
            img.Height = Math.Abs(p1.Y - p2.Y);

            var x = Math.Min(p1.X, p2.X);
            var y = Math.Min(p1.Y, p2.Y);

            return new Drawable(img, new Point(x, y));
        }
    }
}
