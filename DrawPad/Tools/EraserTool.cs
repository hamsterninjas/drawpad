﻿using DrawPad.Classes;
using System;
using System.Windows.Media;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Shapes;
using System.Windows.Controls;
using System.Windows;

namespace DrawPad.Tools
{
    public class EraserTool : StylusTool
    {
        public EraserTool(PixelCanvas canvas, Point startPoint, int strokeThickness) : base(canvas, startPoint, Brushes.White, strokeThickness)
        {
        }

        public override Shape CreateSegment(Point previousPoint, Point nextPoint)
        {
            var rect = new Rectangle
            {
                Width = 10,
                Height = 10,
                Fill = GetColor()
            };

            Canvas.SetLeft(rect, nextPoint.X);
            Canvas.SetTop(rect, nextPoint.Y);

            return rect;
        }
    }
}
