﻿using DrawPad.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace DrawPad.Tools
{
    class ArrowTool : IDrawTool
    {
        public ArrowTool(PixelCanvas canvas) : base(canvas, Brushes.Black, Brushes.Transparent)
        {

        }
        
    }
}
