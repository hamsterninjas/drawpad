﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DrawPad
{
    public partial class BorderThicknessModal : Window
    {
        public int thickness;

        public BorderThicknessModal()
        {
            InitializeComponent();
        }

        public void OkModal_ClickEvent(object sender, RoutedEventArgs e)
        {
            // Dialog box accepted
            this.DialogResult = true;
            GetUserInput();
        }

        private void GetUserInput()
        {
            var userInput = BorderThickness.Text;
            int parsedValue;
            if (!int.TryParse(userInput, out parsedValue))
            {
                MessageBox.Show("This is a number only field");
                return;
            }
            thickness = Int32.Parse(userInput);
        }
    }
}
