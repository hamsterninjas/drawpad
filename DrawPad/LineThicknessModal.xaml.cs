﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace DrawPad
{
    public partial class LineThicknessModal : Window
    {
        public int thickness;

        public LineThicknessModal()
        {
            InitializeComponent();
        }

        public void OkModal_ClickEvent(object sender, RoutedEventArgs e)
        {
            if (!IsValid(this))
            {
                this.DialogResult = false;
            }

            else
            {
                // Dialog box accepted
                this.DialogResult = true;
                GetUserInput();
            }
        }

        private void GetUserInput()
        {
            var userInput = LineThickness.Text;
            int parsedValue;
            if (!int.TryParse(userInput, out parsedValue))
            {
                MessageBox.Show("This is a number only field");
                return;
            }
            thickness = Int32.Parse(userInput);
        }

        // Validate all dependency objects in a window
        bool IsValid(DependencyObject node)
        {
            // Check if dependency object was passed
            if (node == null)
            {
                return false;
            }
            
            if (node != null)
            {
                // Check if dependency object is valid.
                // NOTE: Validation.GetHasError works for controls that have validation rules attached
                bool isValid = !Validation.GetHasError(node);
                if (!isValid)
                {
                    // If the dependency object is invalid, and it can receive the focus,
                    // set the focus
                    if (node is IInputElement)
                    {
                        Keyboard.Focus((IInputElement)node);
                    }

                    return false;
                }
            }

            return true;
        }
    }
}
