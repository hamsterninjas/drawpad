﻿using System.Windows;
using System.Windows.Media;
using System.Windows.Controls;
using System;
using System.Windows.Input;
using DrawPad.Tools;
using DrawPad.Classes;
using System.Windows.Shapes;
using System.Windows.Navigation;
using Xceed.Wpf.Toolkit;
using System.Collections.ObjectModel;

namespace DrawPad
{
    /// <summary>
    /// Interaction logic for CanvasWindow.xaml
    /// </summary>
    public partial class CanvasWindow : Window
    {
        private bool drawing = false;

        private Point previousPoint;
        private PixelCanvas pixelCanvas;
        private IDrawTool currentTool;
        private Point top_LeftPoint = new Point(0, 0);
        private int defaultThickness = 2;
        private int default_ShapeStroke_Thickness = 0;
        private Brush default_ShapeThickness_Color = Brushes.Black;
        private bool strokeColor = false;
        private object NavigationService;

        public CanvasWindow()
        {
            InitializeComponent();
            pixelCanvas = new PixelCanvas(DrawingCanvas);
            currentTool = new SelectorTool(pixelCanvas);
            previousPoint = new Point(0, 0);
        }

        //utility method
        private Point GetPointFromEvent(MouseEventArgs eventArgs)
        {
            var mousePosition_x = eventArgs.GetPosition(DrawingCanvas).X;
            var mousePosition_y = eventArgs.GetPosition(DrawingCanvas).Y;

            return new Point(mousePosition_x, mousePosition_y);
        }

        private void ColorSelector_ClickEvent(object sender, RoutedEventArgs eventArgs)
        {
            // Tag is attribute of button from xaml
            var newColor = PullColorPickerAsBrush(false);
            if (currentTool != null && !(currentTool is PencilTool) && !(currentTool is EraserTool))
            {
                currentTool.SetColor(newColor);
            }
        }

        private void BorderColorPickerTool_SelectedColorChanged(object sender, RoutedPropertyChangedEventArgs<Color?> e)
        {

            if (currentTool is ShapeTool)
            {
                strokeColor = true;
                var newColor = PullColorPickerAsBrush(strokeColor);
                ((ShapeTool)currentTool).strokeColor = newColor;
            }
            strokeColor = false;
        }

        private void BorderThicknessSelector_ClickEvent(object sender, RoutedEventArgs e)
        {

            var modal = new BorderThicknessModal();
            modal.Owner = this;
            modal.ShowDialog();
            var thickness = modal.thickness;
            if (currentTool is ShapeTool)
            {
                ((ShapeTool)currentTool).strokeThickness = thickness;
            }
        }

        private void LineThicknessSelector_ClickEvent(object sender, RoutedEventArgs e)
        {
            var modal = new LineThicknessModal();
            modal.Owner = this;
            modal.ShowDialog();
            var thickness = modal.thickness;
            if (currentTool is StylusTool && !(currentTool is PencilTool))
            {
                ((StylusTool)currentTool).strokeThickness = thickness;
            }
        }

        private Brush PullColorPickerAsBrush(bool stroke)
        {
            var item = new Color();
            var color = GetColor(stroke);

            var a = color.Value.A;
            var r = color.Value.R;
            var g = color.Value.G;
            var b = color.Value.B;

            item.A = a;
            item.R = r;
            item.G = g;
            item.B = b;

            var returnBrush = new SolidColorBrush(item);
            return returnBrush;
        }

        private Color? GetColor(bool stroke)
        {
            Color? selectedColor;

            if (strokeColor || stroke)
            {
                selectedColor = ColorPickerBorder.SelectedColor;
                return selectedColor;
            }
            else
            {
                selectedColor = ColorPickerFill.SelectedColor;
                if (currentTool is ShapeTool && ((ShapeTool)currentTool).strokeThickness != 0)
                {
                    return selectedColor;
                }
                else if (selectedColor.Value.A == 0)
                {
                    ColorPickerFill.SelectedColor = Color.FromArgb(255, 0, 0, 0);
                    return ColorPickerFill.SelectedColor;
                }
                return ColorPickerFill.SelectedColor;
            }
        }

        //   Beginning of tools
        private void EraserTool_ClickEvent(object sender, RoutedEventArgs eventArgs)
        {
            currentTool = new EraserTool(pixelCanvas, top_LeftPoint, defaultThickness);
            ColorPickerFill.Visibility = System.Windows.Visibility.Hidden;
            ColorPickerBorder.Visibility = System.Windows.Visibility.Hidden;
            ShapeBorderButton.Visibility = System.Windows.Visibility.Hidden;
            LineThicknessButton.Visibility = System.Windows.Visibility.Hidden;
        }

        private void PenTool_ClickEvent(object sender, RoutedEventArgs eventArgs)
        {
            // This is default pen

            currentTool = new PenTool(pixelCanvas, top_LeftPoint, PullColorPickerAsBrush(false), defaultThickness);

            ColorPickerFill.Visibility = System.Windows.Visibility.Visible;
            ColorPickerBorder.Visibility = System.Windows.Visibility.Hidden;
            ShapeBorderButton.Visibility = System.Windows.Visibility.Hidden;
            LineThicknessButton.Visibility = System.Windows.Visibility.Visible;
        }

        private void PencilTool_ClickEvent(object sender, RoutedEventArgs eventArgs)
        {
            currentTool = new PencilTool(pixelCanvas, top_LeftPoint);

            ColorPickerFill.Visibility = System.Windows.Visibility.Hidden;
            ColorPickerBorder.Visibility = System.Windows.Visibility.Hidden;
            ShapeBorderButton.Visibility = System.Windows.Visibility.Hidden;
            LineThicknessButton.Visibility = System.Windows.Visibility.Hidden;
        }

        private void BrushTool_ClickEvent(object sender, RoutedEventArgs eventArgs)
        {
            // Code for selecting brush tool
        }

        private void RectangleToolSelector_ClickEvent(object sender, RoutedEventArgs eventArgs)
        {
            currentTool = new RectangleTool(pixelCanvas, PullColorPickerAsBrush(false), true, default_ShapeStroke_Thickness, PullColorPickerAsBrush(true));

            ColorPickerFill.Visibility = System.Windows.Visibility.Visible;
            ColorPickerBorder.Visibility = System.Windows.Visibility.Visible;
            ShapeBorderButton.Visibility = System.Windows.Visibility.Visible;
            LineThicknessButton.Visibility = System.Windows.Visibility.Hidden;
        }

        private void EllipseToolSelector_ClickEvent(object sender, RoutedEventArgs eventArgs)
        {
            currentTool = new EllipseTool(pixelCanvas, PullColorPickerAsBrush(false), true, default_ShapeStroke_Thickness, PullColorPickerAsBrush(true));

            ColorPickerFill.Visibility = System.Windows.Visibility.Visible;
            ColorPickerBorder.Visibility = System.Windows.Visibility.Visible;
            ShapeBorderButton.Visibility = System.Windows.Visibility.Visible;
            LineThicknessButton.Visibility = System.Windows.Visibility.Hidden;
        }

        private void ImageTool_ClickEvent(object sender, RoutedEventArgs eventArgs)
        {
            ColorPickerFill.Visibility = System.Windows.Visibility.Hidden;
            ColorPickerBorder.Visibility = System.Windows.Visibility.Hidden;
            ShapeBorderButton.Visibility = System.Windows.Visibility.Hidden;
            LineThicknessButton.Visibility = System.Windows.Visibility.Hidden;

            //see https://stackoverflow.com/questions/10315188/open-file-dialog-and-select-a-file-using-wpf-controls-and-c-sharp
            Microsoft.Win32.OpenFileDialog dialog = new Microsoft.Win32.OpenFileDialog();

            dialog.DefaultExt = ".png";
            dialog.Filter = "PNG Files (*.png)|*.png";//"JPEG Files (*.jpeg)|*.jpeg|PNG Files (*.png)|*.png|JPG Files (*.jpg)|*.jpg|GIF Files (*.gif)|*.gif";
            dialog.Title = "Open Image";

            Nullable<bool> result = dialog.ShowDialog();

            if (result == true)
            {
                string path = dialog.FileName;
                currentTool = new ImageTool(pixelCanvas, path);
            }
        }

        private void PolygonToolSelector_ClickEvent(object sender, RoutedEventArgs e)
        {
            ColorPickerFill.Visibility = System.Windows.Visibility.Visible;
            ColorPickerBorder.Visibility = System.Windows.Visibility.Visible;
            ShapeBorderButton.Visibility = System.Windows.Visibility.Visible;
            LineThicknessButton.Visibility = System.Windows.Visibility.Hidden;

            var modal = new PolygonModal();
            modal.Owner = this;
            modal.ShowDialog();
            var numOfSides = modal.polygonSides;
            if (numOfSides == 5)
            {
                currentTool = new PentagonTool(pixelCanvas, PullColorPickerAsBrush(false), true, default_ShapeStroke_Thickness, PullColorPickerAsBrush(true));
                ColorPickerFill.Visibility = Visibility.Visible;
            }
            else if (numOfSides == 4)
            {
                currentTool = new RectangleTool(pixelCanvas, PullColorPickerAsBrush(false), true, default_ShapeStroke_Thickness, PullColorPickerAsBrush(true));
                ColorPickerFill.Visibility = Visibility.Visible;
            }
            else if (numOfSides == 3)
            {
                currentTool = new TriangleTool(pixelCanvas, PullColorPickerAsBrush(false), true, default_ShapeStroke_Thickness, PullColorPickerAsBrush(true));
                ColorPickerFill.Visibility = Visibility.Visible;
            }
            else
            {
                currentTool = new PolygonTool(pixelCanvas, PullColorPickerAsBrush(false), true, default_ShapeStroke_Thickness, PullColorPickerAsBrush(true), numOfSides);
                ColorPickerFill.Visibility = Visibility.Visible;
            }

        }

        private void SaveToPNG_ClickEvent(object sender, RoutedEventArgs eventArgs)
        {
            pixelCanvas.SaveToFile();
        }

        //mouse events

        private void MouseButtonDownHandler(object sender, MouseButtonEventArgs eventArgs)
        {
            drawing = true;

            var mousePosition = GetPointFromEvent(eventArgs);

            if (currentTool is StylusTool)
            {
                ((StylusTool)currentTool).DrawStroke(mousePosition, true);
            }
            else if (currentTool is ShapeTool)
            {
                previousPoint = mousePosition;
                ((ShapeTool)currentTool).DrawShape(previousPoint, previousPoint);
            }
        }

        private void MouseButtonUpHandler(object sender, MouseButtonEventArgs eventArgs)
        {
            drawing = false;
            if (currentTool is ShapeTool)
            {
                var mousePosition = GetPointFromEvent(eventArgs);                
                ((ShapeTool)currentTool).Undo();
                ((ShapeTool)currentTool).DrawShape(previousPoint, mousePosition);

                previousPoint = mousePosition;
            }
        }

        private void MouseDownMoveEventHandler(object sender, MouseEventArgs eventArgs)
        {
            var mousePosition = GetPointFromEvent(eventArgs);

            if (drawing)
            {
                if (currentTool is StylusTool)
                {
                    ((StylusTool)currentTool).DrawStroke(mousePosition, false);
                }
                else if (currentTool is ShapeTool)
                {
                    ((ShapeTool)currentTool).UpdateLastShape(previousPoint, mousePosition);
                }
            }
        }

        private void Clear_ClickEvent(object sender, RoutedEventArgs eventArgs)
        {
            pixelCanvas.Clear();
        }

        private void Window_PreviewKeyDownEventHandler(object sender, KeyEventArgs eventArgs)
        {
            //ctrl + Z (undo)
            if (Keyboard.Modifiers == ModifierKeys.Control && eventArgs.Key == Key.Z)
            {
                currentTool.Undo();
            }
            //ctrl + Y (redo)
            if (Keyboard.Modifiers == ModifierKeys.Control && eventArgs.Key == Key.Y)
            {
                currentTool.Redo();
            }
        }

        public void GetCurrentTool(IDrawTool selectedTool)
        {
            currentTool = selectedTool;
        }
    }
}
