﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace DrawPad.Classes
{
    public class PolygonSideSelect : ValidationRule
    {
        private readonly double maxSides = 10;
        private readonly double minSides = 3;

        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            double sides;

            // Is a number?
            if (!double.TryParse((string)value, out sides))
            {
                return new ValidationResult(false, "Not a number.");
            }

            // Is in range?
            if ((sides < maxSides) || (sides > minSides))
            {
                string msg = string.Format("Margin must be between {0} and {1}.", minSides, maxSides);
                return new ValidationResult(false, msg);
            }

            // Number is valid
            return new ValidationResult(true, null);
        }
    }
}
