﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace DrawPad
{
    public class Drawable
    {
        public readonly FrameworkElement element;
        public readonly Point topLeft;

        public Drawable(FrameworkElement element, Point topLeft)
        {
            this.element = element;
            this.topLeft = topLeft;
        }
    }
}
