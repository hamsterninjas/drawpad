using DrawPad.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace DrawPad.Classes
{
    public class UIColorSelector
    {
        private int colorInt;

        public Brush color { get; set; }

        public UIColorSelector(int colorNum)
        {
            colorInt = colorNum;
            color = FillWithColor(colorInt);
        }

        private Brush FillWithColor(int colorInt)
        {
            var selectedColor = (ColorsEnum)colorInt;
            var newColor = new Colors(selectedColor);
            var fill_withColor = newColor.SelectedColor;
            return fill_withColor;
        }
    }
}