﻿using System.ComponentModel.DataAnnotations;

namespace DrawPad.Classes
{
    public enum ColorsEnum
    {
        Black = 0,

        [Display(Name = "Dark Gray")]
        DarkGray = 1,

        [Display(Name = "Light Gray")]
        LightGray = 2,

        Red = 3,

        Yellow = 4,

        Green = 5,

        Blue = 6,

        Purple = 7,

        NoColor = 8,
    }
}
