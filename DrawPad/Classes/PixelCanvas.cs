﻿using System.Windows.Shapes;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using System;
using System.Windows.Input;
using System.Windows;

namespace DrawPad.Classes
{
    public class PixelCanvas
    {
        private readonly Canvas canvas;

        public PixelCanvas(Canvas canvas)
        {
            this.canvas = canvas;
        }
        
        public void Draw(UIElement shape)
        {
            canvas.Children.Add(shape);
        }

        public void Remove(UIElement shape)
        {
            canvas.Children.Remove(shape);
        }

        public void setCursor(Cursor cursor)
        {
            canvas.Cursor = cursor;
        }

        public void Clear()
        {
            canvas.Children.Clear();
        }

        public void SaveToFile()
        {
            //this should be moved somewhere else since it deals with UI

            //see https://stackoverflow.com/questions/10315188/open-file-dialog-and-select-a-file-using-wpf-controls-and-c-sharp
            Microsoft.Win32.SaveFileDialog dialog = new Microsoft.Win32.SaveFileDialog();

            dialog.DefaultExt = ".png";
            dialog.Filter = "PNG Files (*.png)|*.png";//"JPEG Files (*.jpeg)|*.jpeg|PNG Files (*.png)|*.png|JPG Files (*.jpg)|*.jpg|GIF Files (*.gif)|*.gif";

            Nullable<bool> result = dialog.ShowDialog();

            if(result == true)
            {
                string path = dialog.FileName;

                SaveToPNG(path, 96); //dpi shoud be selected through UI at some point it the future
            }
        }

        //see https://blogs.msdn.microsoft.com/saveenr/2008/09/18/wpfxaml-saving-a-window-or-canvas-as-a-png-bitmap/ for more info
        public void SaveToPNG(string path, int dpi)
        {
            RenderTargetBitmap renderTarget = new RenderTargetBitmap((int) canvas.ActualWidth, (int)canvas.ActualHeight, dpi, dpi, System.Windows.Media.PixelFormats.Pbgra32);
            renderTarget.Render(canvas);

            var encoder = new PngBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(renderTarget));
            using (var stm = System.IO.File.Create(path))
            {
                encoder.Save(stm);
            }
        }
    }
}
