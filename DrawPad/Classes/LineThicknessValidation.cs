﻿using System;
using System.Globalization;
using System.Windows.Controls;

namespace DrawPad.Classes
{
    public class LineThicknessValidation : ValidationRule
    {
        public int MaxThickness { get; set; }

        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            int test;
            var valueAsString = value.ToString();
            try
            {
                if (!int.TryParse(valueAsString, out test))
                {
                    return new ValidationResult(false, "Not a number.");
                }

                return new ValidationResult(true, null);
            }

            catch
            {
                return new ValidationResult(false, "Error Occured");
            }
        }
    }
}
