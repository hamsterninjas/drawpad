﻿using System.Windows.Media;
using DrawPad.Classes;

namespace DrawPad.Classes
{
    public class Colors
    {
        public Brush SelectedColor { get; set; }

        public Colors(ColorsEnum selectedColor)
        {
            SelectedColor = SetColor(selectedColor);
        }

        private Brush SetColor(ColorsEnum setColor)
        {
            switch(setColor)
            {
                case ColorsEnum.Black:
                    return Brushes.Black;
                case ColorsEnum.Blue:
                    return Brushes.Blue;
                case ColorsEnum.DarkGray:
                    return Brushes.DarkGray;
                case ColorsEnum.Green:
                    return Brushes.Green;
                case ColorsEnum.LightGray:
                    return Brushes.LightGray;
                case ColorsEnum.Purple:
                    return Brushes.Purple;
                case ColorsEnum.Red:
                    return Brushes.Red;
                case ColorsEnum.Yellow:
                    return Brushes.Yellow;
                case ColorsEnum.NoColor:
                    return Brushes.Transparent;
                default:
                    return Brushes.Black;
            }
        }
    }
}
