﻿using System.Windows.Media;
using DrawPad.Classes;

namespace DrawPad.Classes
{
    public class OurColor 
    {
        public Color  SelectedColor { get; set; }

        public OurColor (ColorsEnum selectedColor)
        {
            SelectedColor = SetColor(selectedColor);
        }

        private Color  SetColor(ColorsEnum setColor)
        {
            switch(setColor)
            {
                case ColorsEnum.Black:
                    return Color.FromRgb(0, 0, 0);
                case ColorsEnum.Blue:
                    return Color.FromRgb(0, 0, 255);
                case ColorsEnum.DarkGray:
                    return Color.FromRgb(30, 30, 30);
                case ColorsEnum.Green:
                    return Color.FromRgb(0, 255, 0);
                case ColorsEnum.LightGray:
                    return Color.FromRgb(150, 150, 150);
                case ColorsEnum.Purple:
                    return Color.FromRgb(200, 0, 200);
                case ColorsEnum.Red:
                    return Color.FromRgb(255, 0, 0);
                case ColorsEnum.Yellow:
                    return Color.FromRgb(0, 200, 200);
                default:
                    return Color.FromRgb(0, 0, 0);
            }
        }
    }
}
