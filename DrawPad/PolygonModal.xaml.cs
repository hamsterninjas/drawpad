﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DrawPad
{
    /// <summary>
    /// Interaction logic for PolygonModal.xaml
    /// </summary>
    public partial class PolygonModal : Window
    {
        public int polygonSides;

        public PolygonModal()
        {
            InitializeComponent();
        }

        public void OkModal_ClickEvent(object sender, RoutedEventArgs e)
        {
            // Dialog box accepted
            this.DialogResult = true;
            GetUserInput();
        }

        private void GetUserInput()
        {
            var userInput = PolgyonSides.Text;
            int parsedValue;
            if (!int.TryParse(userInput, out parsedValue))
            {
                MessageBox.Show("This is a number only field");
                return;
            }
            polygonSides = Int32.Parse(userInput);
        }
    }
}
